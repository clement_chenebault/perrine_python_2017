class Grid:
    def __init__(self, w, h, cols, rows): # Notre methode constructeur



        self.w = w;
        self.h = h;
        self.cols = cols;
        self.rows = rows;

        self.cellWidth = self.w / self.cols; # largeur de la case, largeur diviser par le nombre de colonnes
        self.cellHeight = self.h / self.rows; # pareil, tout ca va etre pratique pour dessiner des dessins de la bonnes taille (par exemple pour les cases acides, etc.)

        self.grid = []; # la grille elle meme quon va creer (pareil qu'une matrice)

        self.createGrid()
        # self.createGrid(self.cols); # on appelle la fonction createGrid
        # self.position.y = 100; # ignorer

        # on va dessiner les cases vides, cest possible de le faire en TKinter aussi, Perrine: pareil que pour la toile d'araignee
        # self.g = new PIXI.Graphics();
        # self.addChild(self.g); # ignorer, on ajoute juste a la scene
        # self.drawGrid(); # cette fonction va dessiner la grille vide

  #   # cette fonction va creer un tableau a deux entrees selon le nombre de colones et de lignes quon veut
    def createGrid(self):
        start = 0;
        end = self.cols;

        for i in range(0, self.cols):
            self.grid.append([])
            # self.grid[i] = []

        # et ici on dit que pour chaque grid[0] cest un autre tableau! grid[0][1]  ======= grid[x][y]
        for i in range(0, self.cols):
            for j in range(0,self.rows):
                self.grid[i].append({"occupied":False, "acid": False, "toxic": False, "object": 0})

        print(self.grid[self.cols - 1][self.rows - 1])


    def resetObject(self, x, y):
        self.grid[x][y]['occupied'] = False;
        self.grid[x][y]['object'] = None;



    def getObject(self, x, y):
        # print('x', x, 'y', y)
        if(x >= self.cols or x < 0 or y < 0 or y >= self.rows): # si on demande  un object en dehors de la grille
            # on renvoie une valeur nulle
            # print('FALSE')
            return False

        return self.grid[x][y];


    def addToxicSurface(self, x, y):
        if(x >= self.cols or x < 0 or y < 0 or y >= self.rows):
            return

        self.grid[x][y]['toxic'] = True


    def addAcidSurface(self, x, y):
        self.grid[x][y]['acid'] = True


    def addObject(self, x, y, obj):
        if(self.grid[x][y]):
            self.grid[x][y]['occupied'] = True; # mainteiant elle est occupee
            self.grid[x][y]['object'] = obj; # et lobjet est stocke

  #
    def debug(self):
        str = ''
        for i in range(0, self.cols):
            for j in range(0, self.rows):
                if self.grid[i][j]['occupied']:
                    if self.grid[i][j]['acid']:
                        str += '  (' + self.grid[i][j]['object'].id + ' | acid) '
                    else:
                        str += '  (' + self.grid[i][j]['object'].id + ') '
                else:
                    if self.grid[i][j]['acid']:
                        str += ' (acid) '
                    else:
                        str += ' NONE '

            str += '\n'

        print (str)


  #   # voila comment on fait une grille (subjective) qui n'en est pas vraiment une!!!
  #   # ce nest pas ce qui dessine la grille, mais cest ce qui contient les donnes de ce quil y aura pour chaque case!
  #   # (ca fait une matrice)
  #
  #
  # # dessiner les traits de la grille, voir dans TKinter comment ca marche mais ca devrait etre ~ la meme chose
  #   def drawGrid(self){
  #       self.g.clear(); # propre a pixi, mais il y a la meme fonction tkinter, ON ENLEVE LE GRAPHIC (voir dans constructeur)
  #       # un graphics est un 'element dessine' (cercle, trait, rectangle, etc.), rien a voir avec les graphiques
  #       self.g.lineStyle(2, 0xffffff); # a quoi ressemble la ligne (pareil dans TKinter a peu pres)
  #
  #       # on dessine d'abord les colones
  #       for (var i = -1; i < self.cols ; i++) { # de -1 a self.cols
  #         # un trait qui va du
  #         self.g.moveTo(i * self.w / self.cols, -self.h / self.rows); # premier point, je vous laisse dessiner sur un carnet comment ca marche
  #         self.g.lineTo(i * self.w / self.cols, self.h - self.h / self.rows); # jusqu'au deuxieme point
  #       }
  #
  #       # pareil pour les lignes, un peu plus faciel a comprendre!
  #       for (var i = -1; i < self.rows ; i++) {
  #         self.g.moveTo(0, i * self.h / self.rows);
  #         self.g.lineTo(self.w, i * self.h / self.rows);
  #       }
  #
  #
  # # un fonction pratique, pour passer d'un systeme de coordonnes de grilles a des positions en pixels, pour placerl les elements a l'ecran
  # # note: la grille est en soit est un tableau qui ne ressemble a rien, on triche un peu mais cest ca le jeu :)
  #   def getRealPos(self, x, y):
  #       return {x: x * self.w / self.cols, y: y * self.h / (self.rows)} # should be straight forward
  #
  #   def addToxicSurface(self, x, y): #  ajoute par le Noyer (dans YearsManager)
  #       if(self, x >= self.cols || x < 0 || y < 0 || y >= self.rows){ # juste un check pour etre sur quon dessine pas en dehors de la grille
  #         return;
  #       }
  #
  #       self.grid[x][y].toxic = true; # si on est dans la grille, on dit que la cellule est toxic
  #
  #   def addAcidSurface(self, x, y):
  #       self.grid[x][y].acid = true; # appele depuis TitleScreen quand on ajoute les surfaces acides
  #
  #     # ajouter un object a une case (caillou, pin, noyer, etc.)
  #     # les parametres sont les coordonnes de la grille, et `obj` est l'objet quon ajoute
  #   def addObject(self, x, y, obj):
  #       self.grid[x][y].occupied = true; # mainteiant elle est occupee
  #       self.grid[x][y].object = obj; # et l'objet est stocke
  #
  #
  # # devrait sappeler `resetCell`, erreur de ma part
  # # une methode pratique pour virer un objet d'une case
  #   def resetObject(self, x, y):
  #       self.grid[x][y].occupied = false;
  #       self.grid[x][y].object = null;
  #
  #
  # # une methode super utile evidemment pour recuperer un element dans la grille
  #   def getObject(self, x, y):
  #       if(x >= self.cols || x < 0 || y < 0 || y >= self.rows){ # si on demande  un object en dehors de la grille
  #         # on renvoie une valeur nulle
  #         return null;
  #       }
  #
  #       # SINON on renvoie la case!
  #       # case = {
  #       #   occupied: true ou false,
  #       #   acid: true ou false,
  #       #   toxic: true ou false,
  #       #   object: null ou Rock() ou Framboisier(), etc.
  #       # }
  #
  #       return self.grid[x][y];
  #
