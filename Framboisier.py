
class Framboisier:
    def __init__(self, generation):
        self.id= "framboisier";
        self.position= {}
        self.positionGrid = {}
        self.dead = False;
        self.generation = generation;

    def nextYear(self):
        if(not self.dead):
            self.generation += 1;


    def die(self):
        self.dead = True
        print('DEAD')


    def onAcid(self):
        self.die()
