from Rock import *;
from Framboisier import *;
from Pin import *;
from Noyer import *;
from Mousse import *;
from Grid import *;

import math
import random


class YearsManager:
    def __init__(self, game):
        self.game = game
        self.grid = self.game.grid

        self.elementsToAdd = []

    def nextYear(self):

        for x in range(0, self.game.rows - 1):
            for y in range(0, self.game.cols - 1):


                caseGrid = self.grid.getObject(x, y);

                if caseGrid['occupied']:
                    obj = caseGrid['object'];

                    if obj.dead:
                        self.grid.resetObject(x, y)
                        continue

                    if caseGrid['acid']:
                        obj.onAcid()

                        if obj.dead:
                            continue

                    if caseGrid['toxic']:
                        obj.die()

                    if obj.id == 'framboisier':
                        self.framboisier(obj)
                    elif obj.id == 'mousse':
                        self.mousse(obj)
                    elif obj.id == 'noyer':
                        self.noyer(obj)
                    elif obj.id == 'pin':
                        self.pin(obj)

        self.addNewObjects()
        # self.game.drawScene()

    def addNewObjects(self):
        for el in self.elementsToAdd:
            obj = el['object']
            x = el['x']
            y = el['y']

            obj.positionGrid['x'] = x;
            obj.positionGrid['y'] = y;

            self.grid.addObject(x, y, obj);
        self.elementsToAdd = [];


    def framboisier(self, obj):
        obj.generation += 1

        for i in range(-1, 2):
            for j in range(-1, 2):
                if (i != 0 or j != 0):
                    newX = obj.positionGrid['x'] + i
                    newY = obj.positionGrid['y'] + j

                    caseGrid = self.grid.getObject(newX, newY);

                    if caseGrid and not caseGrid['occupied'] and not caseGrid['toxic']:
                        framboisier = Framboisier(0);

                        o = {}
                        o['x'] = newX
                        o['y'] = newY
                        o['object'] = framboisier

                        self.elementsToAdd.append(o)


    def mousse(self, obj):
        obj.generation += 1

        for i in range(-1, 2):
            for j in range(-1, 2):
                if (i != 0 or j != 0):
                    newX = obj.positionGrid['x'] + i
                    newY = obj.positionGrid['y'] + j

                    caseGrid = self.grid.getObject(newX, newY);

                    if caseGrid and not caseGrid['occupied'] and not caseGrid['toxic']:
                        framboisier = Mousse(0);

                        o = {}
                        o['x'] = newX
                        o['y'] = newY
                        o['object'] = framboisier

                        self.elementsToAdd.append(o)

    def noyer(self, obj):
        obj.generation += 1
        obj.area += 1;

        if obj.area > 1:
            obj.area = 1

        for x in range(-obj.area, obj.area + 1):
            for y in range(-obj.area, obj.area + 1):

                newX = obj.positionGrid['x'] + x;
                newY = obj.positionGrid['y'] + y;

                if(x != 0 or y != 0):
                    self.grid.addToxicSurface(newX, newY);

                    caseGrid = self.grid.getObject(newX, newY);

                    if caseGrid and caseGrid['occupied'] and caseGrid['object'].id != "rock":
                        if caseGrid['object'].die:
                            caseGrid['object'].die()


    def pin(self, obj):
        obj.generation += 1

        if(obj.generation < 2):
            return

        for i in range(0,2):
            x = math.floor(random.random() * 3) - 1;
            y = 0

            if x== 0:
                if random.random() > .5:
                    y = 1
                else:
                    y = -1
            else:
                y = math.floor(random.random() * 3) - 1;


            caseGrid = self.grid.getObject(x, y);

            if caseGrid and not caseGrid['occupied'] and not caseGrid['toxic']:
                pin = Pin(0);

                o = {}
                o['x'] = x
                o['y'] = y
                o['object'] = pin

                self.elementsToAdd.append(o);
