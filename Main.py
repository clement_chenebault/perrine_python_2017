from tkinter import *

from Rock import *
from Framboisier import *
from Pin import *
from Noyer import *
from Mousse import *
from Grid import *
from YearsManager import *

from View import *
import math
import random

class Main:

    def __init__(self):

        self.w = 600; # largeur de la grille
        self.h = 600; # hauteur
        self.cols = 10; # division sur la largeur
        self.rows = 10; # div sur la hauteur

        # CA Y EST EN CREEE LA GRILLE, et on passe des infos
        # largeur, hauteur, div largeur, div hauteur
        self.grid = Grid(self.w, self.h, self.cols, self.rows);


        # on peuple la grille, les valeurs devront surement etre des inputs de l'utilisateur
        self.addAcideSurface();
        self.randomiseRocks();
        self.randomiseFramboisiers(2);
        self.randomisePins(2);
        self.randomiseNoyers(5);
        self.randomiseMousse(2);

        # self.debugGrid()

        self.yearsManager = YearsManager(self)


        self.view = View();
        # self.debugGrid()

        # le manager d'annee! Important, le manager ca avoir besoin de certaines infos de cette classe
        # du coup on passe "self" en parametre, ce qu irevient a dire:
        # "cree un YearManager, et je me donne en reference si tu as besoin de m'appeler ou de certaines infos a moi" (TitleScreen)
        # self.yearsManager = new YearsManager(self);

        # addChild est une propriete de PIXI.Container, on ajoute a la scene la grille (la grille est aussi un PIXI.Container)
        # avec TKinter, ca sera un peu different du coup ne vous attardez pas trop sur le concept de Container et d'ajouter d'enfant
        # limportant cest qu'on puisse dessiner la grille et la recuperer pour l'afficher a l'ecran, et ca c'est possible
        # self.addChild(self.grid);



        # pareil, ne vous attardez pas trop sur ca, mais l'idee
        # est de creer une scene ou je vais afficher mes trucs dedans (voir TKinter canvas)
        # self.scene = new PIXI.Container();
        # self.scene.position.y = 100;
        # self.addChild(self.scene);
        #
        # self.drawScene();

        self.elements = [];

        self.root = Tk()
        self.canv = Canvas(self.root, width=2000, height=2000)
        self.canv.bind('<Double-1>', self.onCanvasClick)

        self.drawGrid()
        self.canv.pack()
        self.root.mainloop()




    def onCanvasClick(self, event):
        print('click')
        self.yearsManager.nextYear()
        self.clearScene();
        self.drawGrid()
        # self.canv.pack()

    def clearScene(self):
        for element in self.elements:
            self.canv.delete('all')
            element.destroy()

    def drawGrid(self):

        for i in range(0, self.cols):
            self.canv.create_line(i * self.w / self.cols, -self.h / self.rows, i * self.w / self.cols, self.h - self.h / self.rows, fill="#ff0000")

        for i in range(0, self.rows):
            self.canv.create_line(0, i * self.h / self.rows, self.w, i * self.h / self.rows, fill="#ff0000")

        for x in range(0, self.cols):
            for y in range(0, self.rows):
                caseGrid = self.grid.getObject(x, y)
                if(caseGrid):
                    if (caseGrid['acid']):
                        print(x * self.grid.cellWidth, y * self.grid.cellHeight)
                        self.canv.create_rectangle(x * self.grid.cellWidth, y * self.grid.cellHeight, x * self.grid.cellWidth + self.grid.cellWidth, y * self.grid.cellHeight + self.grid.cellHeight, fill="#ffff00")
                #     let acid = new PIXI.Graphics();
                #     acid.beginFill(0xffff00).drawRect(0,0,this.grid.cellWidth, -this.grid.cellHeight)
                #     acid.position = this.grid.getRealPos(x, y);
                #     this.scene.addChild(acid);
                #   }
                    if (caseGrid['toxic']):
                        self.canv.create_rectangle(x * self.grid.cellWidth, y * self.grid.cellHeight, x * self.grid.cellWidth + self.grid.cellWidth, y * self.grid.cellHeight + self.grid.cellHeight, fill="#00ff00")

                    if (caseGrid['occupied']):
                        id = caseGrid['object'].id

                        # if id == 'Mousse':
                        if id == 'rock':
                            photo1 = PhotoImage(file='rock.gif')
                            label = Label(image=photo1)
                            label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                            label.image = photo1 # keep a reference!
                            self.elements.append(label)
                        elif id == 'framboisier':
                            if caseGrid['object'].dead:
                                photo1 = PhotoImage(file='dead_framboisier.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)
                            else:
                                photo1 = PhotoImage(file='framboisier.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)
                        elif id == 'pin':
                            if caseGrid['object'].dead:
                                photo1 = PhotoImage(file='dead_tree.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)
                            else:
                                photo1 = PhotoImage(file='pin.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)
                        elif id == 'noyer':
                            if caseGrid['object'].dead:
                                photo1 = PhotoImage(file='dead_tree.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)
                            else:
                                photo1 = PhotoImage(file='noyer.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)
                        elif id == 'mousse':
                            if caseGrid['object'].dead:
                                photo1 = PhotoImage(file='dead_mousse.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)
                            else:
                                photo1 = PhotoImage(file='mousse.gif')
                                label = Label(image=photo1)
                                label.place(x= x * self.grid.cellWidth, y= y * self.grid.cellHeight)
                                label.image = photo1 # keep a reference!
                                self.elements.append(label)





                        # if(id == ''):
                #     let acid = new PIXI.Graphics();
                #     acid.beginFill(0x00ff00, .3).drawRect(0,0,this.grid.cellWidth, -this.grid.cellHeight)
                #     acid.position = this.grid.getRealPos(x, y);
                #     this.scene.addChild(acid);
                #   }
                  #
                #   if(caseGrid.occupied){
                #     let object = caseGrid.object;
                #     object.position = this.grid.getRealPos(x, y);
                #     object.position.x += this.grid.cellWidth/2;
                #     this.scene.addChild(object);
                #   }

        # photo1 = PhotoImage(file='dead_framboisier.gif')
        #
        # label = Label(image=photo1)
        # label.place(x= 100, y= 200)
        # label.image = photo1 # keep a reference!

        # TO CLEAR THE CANVAS
        # self.canv.delete('all')
        # label.destroy()




    def addAcideSurface(self):
        proportion = math.floor(self.cols * self.rows * 1 / 9);

        # print('proportion', proportion)
        # important: pour commencer, on recupere une case au hasard
        x = random.randint(0, self.cols - 1)
        y = random.randint(0, self.rows - 1)

        #  comme la surface acide doit etre assez homogene, on va pour chaque case acide regarder autours et on ajouter
        #  notre case acide de depart est [x, y]

        whichCaseToLookAround = [
            [x, y]
        ]

        #  un tableau qui contient toutes les cases acides
        whichCaseIsAcid = [
            [x, y] # notre premiere case acide par default
        ]

        while (len(whichCaseIsAcid) < proportion): # tant quon a pas assez de case acide, tu ne sors pas du while
            # print('loop', whichCaseToLookAround)
            futureCasesToLookAround = [];

            for case in whichCaseToLookAround:
                hasAlreadyBeenRefused = False
                currentX = case[0];
                currentY = case[1];

                #   ooo
                #   o1o
                #   ooo

                for i in range(-1, 2):
                    for j in range(-1, 2):
                        if (i != 0 or j != 0):
                            if(len(whichCaseIsAcid) >= proportion):
                                break

                            newX = currentX + i;
                            newY = currentY + j;

                            #  la potentielle future case sera a grid[newX][newY]
                            neighbour = self.grid.getObject(newX, newY) # recuperer la potentielle future case

                            # print('neighbour', neighbour)
                            if neighbour and not neighbour['acid']:
                                # print('INSIDE')
                                # random() > .5 renvoie ~une chance sur deux d'accepeter la case en tant qu'acide,
                                # mais si ca a deja ete refuse avant alors on accepte automatiquement la nouvelle case
                                if random.random() > .5 or hasAlreadyBeenRefused:
                                    self.grid.addAcidSurface(newX, newY) # on dit a la grille, rend cette case acide (rappelez vous que la case est un python dictionnaire: {occupied: false,acid: false,toxic: false })
                                    whichCaseIsAcid.append([newX, newY]) #  ajouter la case avec les autres acides
                                    futureCasesToLookAround.append([newX, newY]) # il faudra qu'on regarde autours de cette case plus tard pour recuperer de nouveaux acides, miam!
                                else:
                                    hasAlreadyBeenRefused = True # on refuse la case en tant que nouvel acide, cette variable est remise a false a la prochaine boucle



                whichCaseToLookAround = futureCasesToLookAround[:] # on copie le tableau
                futureCasesToLookAround = []





    def randomiseFramboisiers(self, nb):
        nbElements = nb;
        nbElementsAdded = 0;

        while (nbElementsAdded < nbElements):
            x = random.randint(0, self.cols - 1)
            y = random.randint(0, self.rows - 1)

            caseGrid = self.grid.getObject(x, y);

            if(not caseGrid['occupied']):
                elem = Framboisier(0);
                elem.positionGrid['x'] = x;
                elem.positionGrid['y'] = y;
                self.grid.addObject(x,y, elem);
                print('add framboisier')
                nbElementsAdded +=1

            pass

    def randomisePins(self, nb):
        nbElements = nb;
        nbElementsAdded = 0;

        while (nbElementsAdded < nbElements):
            x = random.randint(0, self.cols - 1)
            y = random.randint(0, self.rows - 1)

            caseGrid = self.grid.getObject(x, y);

            if(not caseGrid['occupied']):
                elem = Pin(0);
                elem.positionGrid['x'] = x;
                elem.positionGrid['y'] = y;
                self.grid.addObject(x,y, elem);

                nbElementsAdded +=1

            pass


    def randomiseNoyers(self, nb):
        nbElements = nb;
        nbElementsAdded = 0;

        while (nbElementsAdded < nbElements):
            x = random.randint(0, self.cols - 1)
            y = random.randint(0, self.rows - 1)

            caseGrid = self.grid.getObject(x, y);

            if(not caseGrid['occupied']):
                elem = Noyer(0);
                elem.positionGrid['x'] = x;
                elem.positionGrid['y'] = y;
                self.grid.addObject(x,y, elem);

                nbElementsAdded +=1

            pass


    def randomiseMousse(self, nb):
        nbElements = nb;
        nbElementsAdded = 0;

        while (nbElementsAdded < nbElements):
            x = random.randint(0, self.cols - 1)
            y = random.randint(0, self.rows - 1)

            caseGrid = self.grid.getObject(x, y);

            if(not caseGrid['occupied']):
                elem = Mousse(0);
                elem.positionGrid['x'] = x;
                elem.positionGrid['y'] = y;
                self.grid.addObject(x,y, elem);

                nbElementsAdded +=1

            pass


    def debugGrid(self):
        self.grid.debug()

    def randomiseRocks(self):
        numberOfRocks = 10
        nbRocksAdded = 0

        while nbRocksAdded < numberOfRocks:
            x = random.randint(0, self.cols - 1)
            y = random.randint(0, self.rows - 1)

            caseGrid = self.grid.getObject(x, y)
            # print(not caseGrid['occupied'])

            # cet objet a ces proprietes:
            # {
            # occupied: false,
            # acid: false,
            # toxic: false
            # }

            if(not caseGrid['occupied']):
                # on peut y mettre un cailloux
                rock = Rock();
                # print(rock.name)
                rock.positionGrid['x'] = x
                rock.positionGrid['y'] = y
                nbRocksAdded += 1 # pour sortir eventuellement du while
                self.grid.addObject(x,y, rock); # dire a la grille qu'un objet est maintenant a la case [x][y]

            pass




main = Main()
