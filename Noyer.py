
class Noyer:
    def __init__(self, generation):
        self.id= "noyer";
        self.area = 0
        self.position= {}
        self.positionGrid = {}
        self.dead = False;
        self.generation = generation;

    def nextYear(self):
        if(not self.dead):
            self.generation += 1;


    def die(self):
        self.dead = True


    def onAcid(self):
        self.die()
